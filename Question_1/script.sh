#!/bin/sh
cron_sum=$(awk -F: '{print $1 ":" $6}' /etc/passwd | md5sum | awk '{ print $1 } ')
checked_sum=$(cat /var/log/current_users)
dt=`date '+%d/%m/%Y %H:%M:%S'`

if [ "$cron_sum" = "$checked_sum" ]
then
	:
else
	echo "$cron_sum" > "/var/log/current_users"
	echo "$dt changes occurred" >> "/var/log/user_changes"
fi


