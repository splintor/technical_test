Git can be confusing. It's usually done on the command line, and we've all heard some terrifying stories of being this horrible spaghetti monster that if you get wrong, you're in for a world of pain and heartache. Well, I'm here to tell you, thats absolutely true. Ok, Not _entirely_ true. There is some scary stuff that can happen but having some basic workflows, or steps, for yourself will make sure you keep all your precious work safe, and also the stuff you're trying to improve! 

Instead of the usual software development side of this, I'm gonna step back and take it a little closer to home, that Apple Pie your mom used to make, and the one her mom made before that, the one her sister made, and so on! Each one of these are a variation of each other but for arguments sake (and because we all know its true), lets say your mom's pie is the Main one, or the Master if you will! The time rolls around and the receipe has been passed down to you. If you want to get that from her the first time, you can pick up the phone and she'll call it out to you. We won't dig too much into git pull and retrieving a repository for the first time but if you'd like to read more, do check out our tutorial at [this link](https://gitlab.com).

So you have this incredible reciepe but you have a friend that swears using cinnamon in it always makes their pie taste great, and you know yours doesn't have any. So you decide to see can you add it and see how it goes, and so, your first feature addition! Lets create a branch. The command below tells git to make a new branch, based off the one you're on, and call it "cinnamon_time". This is going to be identical to the one you created it from. Its worth noting, all you're doing here is saying "create this", you haven't started working on this new branch.

```
git branch "cinnamon_time"
```

We do this on our command line when we're in the repository or folder for our reciepe. This could be the same as you opening up a page and writing down this new cinnamon trial steps! You tear a page out and start writing  on it. Its time to switch over to the new steps. By using `checkout` we're telling git we want to exclusively work on the "cinnamon_time" variation and leave our `master` branch alone.

```
git checkout "cinnamon_time"
```

So the cooking starts, we work through it, we `add` a little, we remove some stuff and we realise we're really happy! Its time to commit this to memory. 

Firstly, we grab any extra pages we didn't have before. This command tells git to grab all new files and folders it didn't previously know about that now exist!

```
git add .
```

We commit the reciepe to our book of reciepes next with a message saying what it is!

```
git commit -m "added 2 teaspoons of cinnamon and a little extra icing sugar"
```

At this point we can do a `git branch ls` and we'll probably see something like:

```
  master
* cinnamon_time
```

At any moment, we can do a `git checkout master` to swap over to the old reciepe, or ` git checkout cinnamon_time` to take our new one! We can think of this like flicking around our book between the old and the new variation!

We haven't changed our moms old, tried and tested steps, we just added our own and built on hers, just like building our own feature, we've made our own pie that just makes the original that little bit better!

Thanks for reading along and if you've any questions, do hit us up on Twitter at @handle_name or by emailing support on [email]. Happy baking!