

# Question 1
This is a sample script that takes the output of `/etc/passwd` and strips the username and home directory into the following format:

```
username:home_dir
```

It will then convert this to an md5 hash and store it into `/var/log/current_users`. On subsequent runs,  it checks the `current_users` hash to see if it matches the one just run, if they match, no action is taken. If they are different, it updates the `current_users`  file with  the latest hash and writes an entry to `/var/log/user_changes` with the Date and Time of the change in the  format of `DATE TIME changes occurred`.

Finally, we set this as a crontab  entry to run on the zero of each hour using the  following entry in  the users crontab file:

```
0 * * * * /home/gitlab_test/Question_1/script.sh
```

The crontab documentation can be found at https://linux.die.net/man/5/crontab.

## Basic testing
In order to test this, let the script run and as root, run `tail /var/log/current_users`and `/tail/var/log/user_changes`. If the script has run once, it'll have a hash in the first file and an entry in the second. You can then add a user while logged in as root using `useradd gitlab_test_user`. If the script is run again, you can then run the `tail`commands again and you should see the hash has changed and there is another entry in the `user_changes` file. 

## Points of note: 
* The files in `/var/log/` should already exist, if not, either give the running user `su` permissions or create the files as root and give the running user `chown` permissions over them - `sudo chown user_name /var/log/current_users && sudo chown user_name /var/log/user_changes`.
* Depending on your configuration, you may need to run `chmod 755 script.sh` so that that file can be run as a script without permission issues.

# Question 2
The answer for this can be found in `Question_2/answer.md`

# Question 3
The answer for this can be found in `Question_3/answer.md`

# Question 4
The answer for this can be found in `Question_4/answer.md`

# Question 5
The answer for this can be found in `Question_5/answer.md`